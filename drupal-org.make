; REQUIRED ATTRIBUTES
core = 7.x

; API
api = 2

; CORE
projects[drupal][type] = core

; THEMES
projects[] = bootstrap

; MODULES - localization
projects[] = es
projects[] = i10n_client
projects[] = i10n_update

; MODULES - basic
projects[] = features
projects[] = ctools
projects[] = field_slideshow
projects[] = pathauto
projects[] = transliteration
projects[] = token
projects[] = views
projects[] = wysiwyg